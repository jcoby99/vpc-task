## Module 6: VPC

### Sub-task 1 - Configure VPC

**1. The VPC should have a name following this convention <ProjectName>-Network and a CIDR block of 10.0.0.0/16**

![1.1](screenshots/1.1.png)

**2. Create an internet gateway named <ProjectName>-IGW and attach it to the VPC.**

![1.2.1](screenshots/1.2.1.png)

![1.2.2](screenshots/1.2.2.png)


**3. Create public subnet in the first AZ of the VPC**

![1.3.1](screenshots/1.3.1.png)

![1.3.2](screenshots/1.3.2.png)

![1.3.3](screenshots/1.3.3.png)

![1.3.4](screenshots/1.3.4.png)

![1.3.5](screenshots/1.3.5.png)

**4. Create private subnet in the first AZ of the VPC for the Report microservice**

![1.4.1](screenshots/1.4.1.png)

![1.4.2](screenshots/1.4.2.png)

![1.4.3](screenshots/1.4.3.png)

**5. Create RDS subnet in the first AZ of the VPC**

![1.5.1](screenshots/1.5.1.png)

![1.5.2](screenshots/1.5.2.png)

![1.5.3](screenshots/1.5.3.png)

![1.5.4](screenshots/1.5.4.png)

**6. Create a bastion host in the public subnet in the second AZ**

![1.6.1](screenshots/1.6.1.png)

![1.6.2](screenshots/1.6.2.png)

![1.6.3](screenshots/1.6.3.png)

![1.6.4](screenshots/1.6.4.png)

### Sub-task 2 – Connect to resources outside the VPC

**1. Create a VPC Endpoint for SQS**

![2.2](screenshots/2.2.png)

**2. Create a VPC Endpoint for DynamoDB**

![2.3](screenshots/2.3.png)

**3. Display endpoints**

![2.4](screenshots/2.4.png)

### Sub-task 3 – Create EC2 Instances in the VPC

**1. Create EC2 instance with the Main microservice from the previous EC2 Module in the private subnet - <ProjectName>-PublicSubnet-Main**

![3.1](screenshots/3.1.png)

**2. Create EC2 instance with the Report microservice from the previous EC2 Module in the private subnet - <ProjectName>-PrivateSubnet-Report**

![3.2](screenshots/3.2.png)

**3. Create security groups**

![3.3.1](screenshots/3.3.1.png)

![3.3.2](screenshots/3.3.2.png)

![3.3.3](screenshots/3.3.3.png)

![3.3.4](screenshots/3.3.4.png)

![3.3.5](screenshots/3.3.5.png)

![3.3.6](screenshots/3.3.6.png)

**4. Ensure:**

- the application on the public instance is available from anywhere

![3.4.1](screenshots/3.4.1.png)

- the private instances are available from the bastion ONLY when you’re connected to it over - SSH (use the ping command or also ssh them)

![3.4.2](screenshots/3.4.2.png)

- the bastion host and public instance have access to the Internet (ping Google, for example)

![3.4.3](screenshots/3.4.3.png)

- the private instance has no any access to the Internet (ping Google, for example)

![3.4.4](screenshots/3.4.4.png)

- the private and public instances have access to each other (ping again)

![3.4.5](screenshots/3.4.5.png)

